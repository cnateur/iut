package assurances

class UrlMappings {

    static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        "/"(controller: "assurance",action: "connection")
        "500"(view:'/error')
        "404"(view:'/notFound')
    }
}
