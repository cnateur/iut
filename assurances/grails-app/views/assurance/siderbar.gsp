

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
    <asset:stylesheet src="siderbar.css"/>
</head>

<body>

    <div class="wrapper">
        <nav  id="sidebar">
            <div class="siderbar-header">

                <h3>REGISTRE</h3>
            </div>

            <ul class="list-unstyled components">

                <p>Dummy heading</p>
                <li class="active">

                    <a href="#homesubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">page</a>
                            <ul class="collapse list-unstyled" id="homesubmenu">

                                <li>
                                    <a href="#">home1</a>
                                </li>
                                <li>
                                    <a href="#">Home2</a>
                                </li>
                                <li>
                                    <a href="#">home3</a>
                                </li>

                            </ul>
                    <li>

                <li>
                <a  href="#">About</a>
            </li>

                <li class="active">

                    <a href="#homesubmenu2" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Dashbord</a>
                    <ul class="collapse list-unstyled" id="homesubmenu2">

                        <li>
                            <a href="#">home1</a>
                        </li>
                        <li>
                            <a href="#">Home2</a>
                        </li>
                        <li>
                            <a href="#">home3</a>
                        </li>

                    </ul>
                <li>
                <li>
                <a href="#homesubmenu2" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Vehicule</a>
                <ul class="collapse list-unstyled" id="homesubmenu3">

                    <li>
                        <a href="#">home1</a>
                    </li>
                    <li>
                        <a href="#">Home2</a>
                    </li>
                    <li>
                        <a href="#">home3</a>
                    </li>

                </ul>
                </li>
                <li>
                    <a href="#homesubmenu2" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">assurance</a>
                    <ul class="collapse list-unstyled" id="homesubmenu4">

                        <li>
                            <a href="#">home1</a>
                        </li>
                        <li>
                            <a href="#">Home2</a>
                        </li>
                        <li>
                            <a href="#">home3</a>
                        </li>

                    </ul>
                </li>

                <li>
                    <a href="#homesubmenu2" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Echeance</a>
                    <ul class="collapse list-unstyled" id="homesubmenu5">

                        <li>
                            <a href="#">home1</a>
                        </li>
                        <li>
                            <a href="#">Home2</a>
                        </li>
                        <li>
                            <a href="#">home3</a>
                        </li>

                    </ul>
                </li>
            </ul>

            <ul class="list-unstyled CTAs">
                <li>
                    <a href="#" class="download">paramaitre</a>
                </li>
                <li>
                    <a href="#" class="article">fermer session</a>
                </li>
            </ul>
        </nav>
    </div>

<asset:javascript src="application.js"/>
<asset:javascript src="sidebar.js"/>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>

</body>
</html>