

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
    <asset:stylesheet src="application.css"/>
    <asset:stylesheet src="navbar_style.css"/>
    <meta name="viezport" content="width=device-width, initial-scale=1" charset="utf-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <script type="text/javascript" src="js/bootstrap.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:ital,wght@1,600;1,900&display=swap" rel="stylesheet">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>

<body>


<div class="container-fluid bg">
    <div class="row">
        <div class="col-md-4 col-ms-4 col-xs-12"></div>

        <div class="col-md-4 col-ms-4 col-xs-12">
            <form class="form-container" method="post" action="/assurance/index" name="form1">
                <h1 class="h1">sign in</h1>
                <div class="clearfix">

                    <!--<a href="pages-recover-password.php" class="pull-right">Lost Password?</a>-->
                </div>
                <div class="form-group input-group-icon">
                    <i class="fa fa-envelope-o" aria-hidden="true"></i>
                    <input type="text" name="num-client" placeholder="email" class="form-control input-lg" required>

                </div>


                <div class="form-group">
                    <i class="fa fa-lock" aria-hidden="true"></i>
                    <input type="text" name="num-client" placeholder="password" class="form-control" >
                </div>

                <div class="form-inline">
                    <g:checkBox name="remember" value="remember"/><label id="remenber"> ce souvenir</label>
                </div>
                <label class="lab" name="forgot" id="forgot" >password  <a href="#" >  forgot</a></label>

                <button class="btn btn-primary " type="submit" name="Validez">connecter</button>

                <p class=" h6 ppara">create <a href="/assurance/inscription" > sign up</a></p>
            </form>

        </div>

        <div class="col-md-4 col-ms-4 col-xs-12"></div>

    </div>


    <asset:javascript src="application.js"/>
    <asset:javascript src="sidebar.js"/>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>


</body>
</html>