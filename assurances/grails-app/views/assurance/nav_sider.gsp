

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
    <asset:stylesheet src="siderbar.css"/>
    <asset:stylesheet src="navbar_style.css"/>
</head>

<body>
<nav class="navbar navbar-expand-md navbar-light bg-dark sticky-top">
    <div class="container-fluid">
        <button type="button" id="siderbarcollapse" class="navbar-toggler">
            <i class="fa fa-align-justify"></i><span></span>
        </button>

        <a class="navbar-brand" href="#"><h1 class="text-light"><a href="#intro" class="scrollto"><span style="color: #FFFF00;">Africa</span><span style="color: 	#FF1493;">Negro</span></a></h1></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive">
            <span class="navbar-toggler-icon"></span></button>

        <div class="collapse navbar-collapse" id="navbarResponsive">


        </div>
    </div>
</nav>


<section class="menu">

    <div class="wrapper">
        <nav  id="sidebar">
            <div class="siderbar-header">

                <h3>REGISTRE</h3>
            </div>

            <ul class="list-unstyled components">

                <p>Dummy heading</p>
                <li class="active">

                    <a href="#homesubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">dashbord</a>
                    <ul class="collapse list-unstyled" id="homesubmenu">

                        <li>
                            <a href="#">home1</a>
                        </li>
                        <li>
                            <a href="#">Home2</a>
                        </li>
                        <li>
                            <a href="#">home3</a>
                        </li>

                    </ul>
                </li>

                <li class="active">
                <a href="#homesubmenu2" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Dashbord</a>
                <ul class="collapse list-unstyled" id="homesubmenu2">
                    <li>
                        <a href="#">home1</a>
                    </li>
                    <li>
                        <a href="#">Home2</a>
                    </li>
                    <li>
                        <a href="#">home3</a>
                    </li>
            </ul>
                </li>

                <li class="active">
                    <a href="#homesubmenu3" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Dashbord</a>
                    <ul class="collapse list-unstyled" id="homesubmenu3">
                        <li>
                            <a href="#">home1</a>
                        </li>
                        <li>
                            <a href="#">Home2</a>
                        </li>
                        <li>
                            <a href="#">home3</a>
                        </li>
                    </ul>
                </li>

            <ul class="list-unstyled CTAs">
                <li>
                    <a href="#" class="download">paramaitre</a>
                </li>
                <li>
                    <a href="#" class="article">fermer session</a>
                </li>
            </ul>
        </nav>
    </div>
</section>
<script>

    $(document).ready(function () {
        $('#siderbarcollapse').on('click', function () {

            $('#sidebar').toggleClass('active');

        });
    });
</script>
<asset:javascript src="application.js"/>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
</body>
</html>