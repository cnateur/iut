

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
    <asset:stylesheet src="application.css"/>
    <meta name="viezport" content="width=device-width, initial-scale=1" charset="utf-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <script type="text/javascript" src="js/bootstrap.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="styles.css">

</head>

<body name="bg" id="bg">
<g:include action="nav.gsp"/>

<section id="hero">


    <div class="carousel slide" data-ride="carousel" id="slides">
        <ul class="carousel-indicators">
            <li data-target="#slides" data-slide-to="0" class="active"></li>
            <li data-target="#slides" data-slide-to="1"></li>
            <li data-target="#slides" data-slide-to="2"></li>
            <li data-target="#slides" data-slide-to="3"></li>
            <li data-target="#slides" data-slide-to="4"></li>
        </ul>
        <div class="carousel-inner">
            <div class="carousel-item  active">
                <img src="kk.jpg" class="img_int">

                <div class="hero-container">

                    <h1 class="h1" id="titre">bienvenue dans nos activiter de formation</h1>
                    <p class="h3" id="para">le travail ces la cle de reuisite</p>
                    <a class="btn btn-outline-light" href=/assurance/dashbord>commencer</a>

                </div>
            </div>

            <div class="carousel-item">
                <img src="../assets/images/kk.jpg">
                <div class="carousel-caption">
                    <h1 class="display-2">AfricaNegro</h1>
                    <h3> je suis webmaster</h3>
                    <button type="button" class="btn btn-outline-light btn-lg">View Demo</button>
                    <button type="button" class="btn btn-primary btn-lg">get starte</button>
                </div>
            </div>

            <div class="carousel-item">
                <img src="images/sss2.jpg">
                <div class="carousel-caption">
                    <h1 class="display-2">AfricaNegro</h1>
                    <h3> je suis webmaster</h3>
                    <button type="button" class="btn btn-outline-light btn-lg">View Demo</button>
                    <button type="button" class="btn btn-primary btn-lg">get starte</button>
                </div>
            </div>

            <div class="carousel-item">
                <img src="images/sss5.jpg">
                <div class="carousel-caption">
                    <h1 class="display-2">AfricaNegro</h1>
                    <h3> je suis webmaster</h3>
                    <button type="button" class="btn btn-outline-light btn-lg">View Demo</button>
                    <button type="button" class="btn btn-primary btn-lg">get starte</button>
                </div>
            </div>

            <div class="carousel-item">
                <img src="images/slider54.jpg">
                <div class="carousel-caption">
                    <h1 class="display-2">AfricaNegro</h1>
                    <h3> je suis webmaster</h3>
                    <button type="button" class="btn btn-outline-light btn-lg">View Demo</button>
                    <button type="button" class="btn btn-primary btn-lg">get starte</button>
                </div>
            </div>
        </div>
    </div>

</section>


</div>



<asset:javascript src="application.js"/>
</body>
</html>