<%--
  Created by IntelliJ IDEA.
  User: bruno
  Date: 10/06/2020
  Time: 01:57
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
  <title></title>
  <asset:stylesheet src="navbar_style.css"/>

</head>
<body>
<nav class="navbar navbar-expand-md navbar-light bg-dark sticky-top">
  <div class="container-fluid">
    <button type="button" id="siderbarcollapse" class="navbar-toggler">
      <i class="fa fa-align-justify"></i><span></span>
    </button>

    <a class="navbar-brand" href="#"><h1 class="text-light"><a href="#intro" class="scrollto"><span style="color: #FFFF00;">Africa</span><span style="color: 	#FF1493;">Negro</span></a></h1></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive">
      <span class="navbar-toggler-icon"></span></button>

    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav ml-auto" id="item">
        <li class="nav-item  active"><a class="nav-link"  href="#">Acceuil</a></li>
        <li class="nav-item  active"><a class="nav-link" href="#" >Apropos</a></li>
        <li class="nav-item active"><a class="nav-link" href="#" >service</a></li>
        <li class="nav-item active"><a class="nav-link" href="#" >Equipes</a></li>
        <li class="nav-item active"><a class="nav-link" href="#" >Contact</a></li>

      </ul>

    </div>
  </div>
</nav>
  <script>

    $(document).ready(function () {
      $('#siderbarcollapse').on('click', function () {

        $('#sidebar').toggleClass('active');

      });
    });
  </script>
<asset:javascript src="application.js"/>
</body>
</html>